# Ansible role for docker installation

Supported platforms:

- Ubuntu 16.04+
- Red Hat based (Centos, RHEL)

Usage example:

```yaml
- hosts: mydockerhosts

  roles:
  - role: docker
    vars:
      docker_update_channel: test
      docker_data_dir: /home/dockerdata
      python_compose_module: true
```
